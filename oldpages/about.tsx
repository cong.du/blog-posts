import Head from "next/head";
import React from "react";

interface IProps {
  title: string;
  description: string;
}

function About ({ title, description }: IProps) {
  return <>
    <Head>
      <title>{title}</title>
      <meta name="description" content={description} />
    </Head>
    <div className="content">About page</div>
  </>
}

export default About;

About.getLayout = function PageLayout(page: React.ReactNode) {
  return (
    <>
      {page}
    </>
  )
}

export async function getStaticProps () {
  return {
    props: {
      title: 'About Page',
      description: "About Description"
    }
  }
}
