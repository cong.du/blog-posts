import * as variablesIcon from '@/public/icons';
import React from 'react';

export default function IconPage () {
  const iconsList = (() => {
    const iconsName = Object.keys(variablesIcon);
    return iconsName.map(name => {
      const Icon = (variablesIcon as any)[name as string];
      const displayName = process.env.NODE_ENV !== 'production' ? name : '';

      if (!Icon) return <React.Fragment key={name}></React.Fragment>;
      return (
        <div
          key={name}
          className="flex h-full min-h-[90px] flex-col items-center justify-center rounded bg-gray-100 p-1 text-black"
        >
          <Icon width="20" />
          <span className="mt-2">{displayName}</span>
        </div>
      );
    });
  })();

  return (
    <div className="grid grid-cols-4 grid-rows-[auto] items-center gap-4 md:grid-cols-8 lg:grid-cols-12">
      {iconsList}
    </div>
  )
}
