import Image from 'next/image'
import { Inter } from 'next/font/google'

const inter = Inter({ subsets: ['latin'] })

export default function Home() {
  console.log('env ', process.env)
  return (
    <main
      className={`flex min-h-screen flex-col items-center justify-between p-24 ${inter.className}`}
    >
      Build web test ci/cd. Port {process.env?.NEXT_PUBLIC_PORT}.
    </main>
  )
}
