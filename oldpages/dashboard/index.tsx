import DashboardLayout from '@/components/layouts/dashboard';

export default function Dashboard({}) {
  return (
    <>
      This is content dashboard
    </>
  );
}

Dashboard.getLayout = function getLayout(page: React.ReactElement) {
  return (
    <DashboardLayout>{page}</DashboardLayout>
  )
}
