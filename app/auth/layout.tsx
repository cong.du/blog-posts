import AuthLayout from "@/components/layouts/auth";

export default function Layout({ children }: { children: React.ReactNode; }) {
  return <AuthLayout>{children}</AuthLayout>
}