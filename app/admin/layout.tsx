import AdminLayout from "@/components/layouts/dashboard";

export default function Layout({ children }: { children: React.ReactNode; }) {
    return <AdminLayout>{children}</AdminLayout>
}