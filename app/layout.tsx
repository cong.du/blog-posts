import ClientLayout from "@/components/layouts/client"
import { Metadata } from "next"
import { firebase } from "@/service/firebase/firebase"
// These styles apply to every route in the application
import "@/assets/css/globals.css"

firebase();

export const metadata: Metadata = {
    title: 'Home',
    description: 'Welcome to Next.js'
}

export default function RootLayout({
    // Layouts must accept a children prop.
  // This will be populated with nested layouts or pages
  children,
}: {
    children: React.ReactNode
}) {
    return (
        <html lang="en">
          <body>
            {children}
          </body>
        </html>
      )
};