import Content from "./Content";
import LeftNavbar from "./LeftNavbar";

export default function AdminLayout({ children }: any) {
  return (
    <div className="bg-white">
      <LeftNavbar />
      <Content>{children}</Content>
    </div>
  )
}