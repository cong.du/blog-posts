import LeftNavbar from "./LeftNavbar";
import RightNavbar from "./RightNavbar";
import Content from "./Content";

export default function ClientLayout({ children }: any) {
  return (
    <>
      <LeftNavbar />
      <Content>{children}</Content>
      <RightNavbar />
    </>
  )
}