import dynamic from 'next/dynamic'

export const DashboardIcon = dynamic(() => import('@/public/icons/dashboard.svg'), { loading: () => <p>Loading...</p> });
