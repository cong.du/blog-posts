import dynamic from 'next/dynamic';

type SVGProps = React.ComponentType<any>;

const Loading = () => <p>Loading...</p>

export const DashboardIcon: SVGProps = dynamic(() => import('./dashboard.svg'), { loading: Loading });
export const MenuIcon: SVGProps = dynamic(() => import('./menu.svg'), { loading: Loading });
export const KanbanIcon: SVGProps = dynamic(() => import('./kanban.svg'), { loading: Loading });
export const InboxIcon: SVGProps = dynamic(() => import('./inbox.svg'), { loading: Loading });
export const UserIcon: SVGProps = dynamic(() => import('./users.svg'), { loading: Loading });
export const ProductIcon: SVGProps = dynamic(() => import('./product.svg'), { loading: Loading });
export const SignInIcon: SVGProps = dynamic(() => import('./signin.svg'), { loading: Loading });
export const SignUpIcon: SVGProps = dynamic(() => import('./signup.svg'), { loading: Loading });
