import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import { getAuth, createUserWithEmailAndPassword, signOut } from "firebase/auth";

export const firebaseConfig = {
    apiKey: process.env.API_KEY,
    authDomain: process.env.AUTH_DOMAIN,
    projectId: process.env.PROJECT_ID,
    storageBucket: process.env.STORAGE_BUCKET,
    messagingSenderId: process.env.MESSAGING_SENDER_ID,
    appId: process.env.APP_ID,
    measurementId: process.env.MEASUREMENT_ID
}

let initialFirebase: any;
let analytics;

// Initialize firebase
export const firebase = () => {
    if (!initialFirebase && typeof window !== 'undefined') {
        initialFirebase = initializeApp(firebaseConfig);
        analytics = getAnalytics(initialFirebase);
    }
    
    if (initialFirebase?.name && typeof window !== 'undefined') {
        analytics = getAnalytics(initialFirebase);
      }
    return initialFirebase;
}
// Registration user
export const createUserWithEmailPasswordFormFirebase = (email: string, password: string) => {
    const auth = getAuth();
    return createUserWithEmailAndPassword(auth, email, password);
}

export const signOutFirebase = () => {const auth = getAuth();signOut(auth);}